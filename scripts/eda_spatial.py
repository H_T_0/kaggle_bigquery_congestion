#!/usr/bin/env python
# coding: utf-8

# ### DATA DESCRIPTION

# The data consists of aggregated trip logging metrics from commercial vehicles, such as semi-trucks. The data have been grouped by intersection, month, hour of day, direction driven through the intersection, and whether the day was on a weekend or not.
# 
# For each grouping in the test set, you need to make predictions for three different quantiles of two different metrics covering how long it took the group of vehicles to drive through the intersection. Specifically, the 20th, 50th, and 80th percentiles for the total time stopped at an intersection and the distance between the intersection and the first place a vehicle stopped while waiting. You can think of your goal as summarizing the distribution of wait times and stop distances at each intersection.
# 
# Each of those six predictions goes on a new row in the submission file. Read the submission TargetId fields, such as 1_1, as the first number being the RowId and the second being the metric id. You can unpack the submission metric id codes with submission_metric_map.json.
# 
# The training set includes an optional additional output metric (TimeFromFirstStop) in case you find that useful for building your models. It was only excluded from the test set to limit the number of predictions that must be made.

# ### SCOPE OF THIS NOTEBOOK

# - EDA by spatial and temporal visualisation of the training data

# ### Import modules and load training data

# In[ ]:


import os
import pandas as pd
import plotly
import plotly.express as px
from keplergl import KeplerGl
from datetime import datetime
import json


# In[ ]:


pd.set_option('display.max_columns', None)


# In[ ]:


plotly.offline.init_notebook_mode()


# In[ ]:


px.set_mapbox_access_token('pk.eyJ1IjoiaHRhazEwIiwiYSI6ImNqZTB4a3RqazY1ajgyeHFocnk0b2NpOTUifQ.JL8YVn1zYcN3669YPLjohg')


# In[ ]:


os.listdir('../data/bigquery-geotab-intersection-congestion')


# In[ ]:


df_train = pd.read_csv('../data/bigquery-geotab-intersection-congestion/train.csv')


# ### Data Overview

# In[ ]:


df_train.head()


# In[ ]:


df_train.info()


# In[ ]:


for c in df_train.columns:
    if len(df_train[df_train[c].isna()==False]) < len(df_train):
        print('NULL EXIST IN COLUMN:', c)


# In[ ]:


df_train.describe()


# #### SPATIAL - TotalTimeStopped_p50

# In[ ]:


# Prepare dataframe to plot on map
df_train_time_spatial = df_train.groupby(['IntersectionId','Hour','City'], as_index=False).agg({
    'TotalTimeStopped_p50': 'max', 'Latitude': lambda x: x.iloc[0], 'Longitude': lambda x: x.iloc[0]
}).sort_values(['City']).reset_index(drop=True)
df_train_time_spatial['Time'] = pd.Series([str(datetime(2019, 1, 1, h)) for h in df_train_time_spatial['Hour']])


# In[ ]:


# Load keplergl map config for each city
keplergl_config_files = [config for config in os.listdir() if config[-4:] == 'json']
configs = {}
for config_file in keplergl_config_files:
    with open(config_file, 'r') as config_json:
        city = config_file.split('.')[0].split('_')[-1]
        configs[city] = json.load(config_json)


# In[ ]:


df_train_time_spatial_Atlanta = KeplerGl(
    data={'Atlanta': df_train_time_spatial[df_train_time_spatial['City'] == 'Atlanta']}, height=1000, config=configs['Atlanta']
)
df_train_time_spatial_Atlanta


# In[ ]:


df_train_time_spatial_Boston = KeplerGl(
    data={'Boston': df_train_time_spatial[df_train_time_spatial['City'] == 'Boston']}, height=1000, config=configs['Boston']
)
df_train_time_spatial_Boston


# In[ ]:


df_train_time_spatial_Chicago = KeplerGl(
    data={'Chicago': df_train_time_spatial[df_train_time_spatial['City'] == 'Chicago']}, height=1000, config=configs['Chicago']
)
df_train_time_spatial_Chicago


# In[ ]:


df_train_time_spatial_Philadelphia = KeplerGl(
    data={'Philadelphia': df_train_time_spatial[df_train_time_spatial['City'] == 'Philadelphia']}, height=1000, config=configs['Philadelphia']
)
df_train_time_spatial_Philadelphia


# In[ ]:


# # Save map configurations
# config_cities = {
#     'Atlanta': df_train_time_spatial_Atlanta.config, 
#     'Boston': df_train_time_spatial_Boston.config, 
#     'Chicago': df_train_time_spatial_Chicago.config, 
#     'Philadelphia': df_train_time_spatial_Philadelphia.config
# }

# for city, config in config_cities.items():
#     with open(f'./config_keplergl_{ city }.json', 'w') as config_json:
#         json.dump(config, config_json, indent=4)


# In[ ]:


# GENERAL NOTES & FINDINGS

# TOTAL TIME STOPPED:
# Chicago seems to be busy only in day time
# Intersections along major corridors seem to have high delays - PATH CAN BE USEFUL DATA
# City centres are not necessarily have higher delays


# ### CONTINUE TO "data_preprocessing.ipynb"
