#!/usr/bin/env python
# coding: utf-8

# ### DATA DESCRIPTION

# The data consists of aggregated trip logging metrics from commercial vehicles, such as semi-trucks. The data have been grouped by intersection, month, hour of day, direction driven through the intersection, and whether the day was on a weekend or not.
# 
# For each grouping in the test set, you need to make predictions for three different quantiles of two different metrics covering how long it took the group of vehicles to drive through the intersection. Specifically, the 20th, 50th, and 80th percentiles for the total time stopped at an intersection and the distance between the intersection and the first place a vehicle stopped while waiting. You can think of your goal as summarizing the distribution of wait times and stop distances at each intersection.
# 
# Each of those six predictions goes on a new row in the submission file. Read the submission TargetId fields, such as 1_1, as the first number being the RowId and the second being the metric id. You can unpack the submission metric id codes with submission_metric_map.json.
# 
# The training set includes an optional additional output metric (TimeFromFirstStop) in case you find that useful for building your models. It was only excluded from the test set to limit the number of predictions that must be made.

# ### SCOPE OF THIS NOTEBOOK

# - EDA for numerical data in the training data

# ### Import modules and load training data

# In[ ]:


import os
import pandas as pd
import plotly
import plotly.express as px
from keplergl import KeplerGl
from datetime import datetime
import json


# In[ ]:


pd.set_option('display.max_columns', None)


# In[ ]:


plotly.offline.init_notebook_mode()


# In[ ]:


px.set_mapbox_access_token('pk.eyJ1IjoiaHRhazEwIiwiYSI6ImNqZTB4a3RqazY1ajgyeHFocnk0b2NpOTUifQ.JL8YVn1zYcN3669YPLjohg')


# In[ ]:


os.listdir('../data/bigquery-geotab-intersection-congestion')


# In[ ]:


df_train = pd.read_csv('../data/bigquery-geotab-intersection-congestion/train.csv')


# ### Data Overview

# In[ ]:


df_train.head()


# In[ ]:


df_train.info()


# In[ ]:


for c in df_train.columns:
    if len(df_train[df_train[c].isna()==False]) < len(df_train):
        print('NULL EXIST IN COLUMN:', c)


# In[ ]:


df_train.describe()


# ### EDA for Numerical Data

# #### TotalTimeStopped

# In[ ]:


# px.histogram(
#     df_train[df_train['TotalTimeStopped_p20']>0], 
#     x='TotalTimeStopped_p20', facet_row='City', color='City', barmode='overlay'
# )


# In[ ]:


px.histogram(
    df_train[df_train['TotalTimeStopped_p50']>0], 
    x='TotalTimeStopped_p50', facet_row='City', color='City', barmode='overlay'
)


# In[ ]:


# px.histogram(
#     df_train[df_train['TotalTimeStopped_p80']>0], 
#     x='TotalTimeStopped_p80', facet_row='City', color='City', barmode='overlay'
# )


# #### TimeFromStopped

# In[ ]:


# px.histogram(
#     df_train[df_train['TimeFromFirstStop_p20']>0], 
#     x='TimeFromFirstStop_p20', facet_row='City', color='City', barmode='overlay'
# )


# In[ ]:


px.histogram(
    df_train[df_train['TimeFromFirstStop_p50']>0], 
    x='TimeFromFirstStop_p50', facet_row='City', color='City', barmode='overlay'
)


# In[ ]:


# px.histogram(
#     df_train[df_train['TimeFromFirstStop_p80']>0], 
#     x='TimeFromFirstStop_p80', facet_row='City', color='City', barmode='overlay'
# )


# #### DistanceToFirstStop

# In[ ]:


# px.histogram(
#     df_train[df_train['DistanceToFirstStop_p20']>0], 
#     x='DistanceToFirstStop_p20', facet_row='City', color='City', barmode='overlay'
# )


# In[ ]:


px.histogram(
    df_train[df_train['DistanceToFirstStop_p50']>0], 
    x='DistanceToFirstStop_p50', facet_row='City', color='City', barmode='overlay'
)


# In[ ]:


# px.histogram(
#     df_train[df_train['DistanceToFirstStop_p80']>0], 
#     x='DistanceToFirstStop_p80', facet_row='City', color='City', barmode='overlay'
# )


# ### Relationship with StoppedTime

# In[ ]:


df_train.columns


# In[ ]:


df_train.groupby(['Hour','IntersectionId','EntryHeading','ExitHeading'], as_index=False)['TotalTimeStopped_p50'].max()


# In[ ]:


px.bar(
    df_train.groupby(['Hour','City'], as_index=False)['TotalTimeStopped_p50'].max().sort_values(['City']), 
    x='Hour', y='TotalTimeStopped_p50', facet_row='City', color='City'
).update_layout(title='Max of TotalTimeStopped_p50 by Hour by City')


# In[ ]:


px.bar(
    df_train.groupby(['Month','City'], as_index=False)['TotalTimeStopped_p50'].max().sort_values(['City']), 
    x='Month', y='TotalTimeStopped_p50', facet_row='City', color='City'
).update_layout(title='Max of TotalTimeStopped_p50 by Month by City')


# In[ ]:


px.bar(
    df_train.groupby(['Weekend','Hour','City'], as_index=False)['TotalTimeStopped_p50'].max().sort_values(['City']), 
    x='Hour', y='TotalTimeStopped_p50', facet_row='City', facet_col='Weekend', color='City'
).update_layout(title='Max of TotalTimeStopped_p50 by Weekend & Hour by City')


# ### Relationship with DistanceStop

# In[ ]:


px.bar(
    df_train.groupby(['Hour','City'], as_index=False)['DistanceToFirstStop_p50'].max().sort_values(['City']), 
    x='Hour', y='DistanceToFirstStop_p50', facet_row='City', color='City'
).update_layout(title='Max of DistanceToFirstStop_p50 by Hour by City')


# In[ ]:


px.bar(
    df_train.groupby(['Month','City'], as_index=False)['DistanceToFirstStop_p50'].max().sort_values(['City']), 
    x='Month', y='DistanceToFirstStop_p50', facet_row='City', color='City'
).update_layout(title='Max of DistanceToFirstStop_p50 by Month by City')


# In[ ]:


px.bar(
    df_train.groupby(['Weekend','Hour','City'], as_index=False)['DistanceToFirstStop_p50'].max().sort_values(['City']), 
    x='Hour', y='DistanceToFirstStop_p50', facet_row='City', facet_col='Weekend', color='City'
).update_layout(title='Max of DistanceToFirstStop_p50 by Weekend & Hour by City')


# ### MOVEMENTS

# In[ ]:


df_train_move = df_train.groupby(
    ['IntersectionId','EntryHeading','ExitHeading','City'], as_index=False
)[['TotalTimeStopped_p50','DistanceToFirstStop_p50']].max().sort_values('City').reset_index(drop=True)


# In[ ]:


df_train_move


# In[ ]:


plotly.subplots.make_subplots(
    rows=2, cols=2, shared_xaxes=True
).add_trace(
    px.scatter(
        df_train_move[df_train_move['City'] == 'Atlanta'], 
        x='TotalTimeStopped_p50', y='DistanceToFirstStop_p50', color='City', opacity=0.3, color_discrete_map={'Atlanta':'blue'}
    )['data'][0], row=1, col=1
).add_trace(
    px.scatter(
        df_train_move[df_train_move['City'] == 'Boston'], 
        x='TotalTimeStopped_p50', y='DistanceToFirstStop_p50', color='City', opacity=0.3, color_discrete_map={'Boston':'red'}
    )['data'][0], row=1, col=2
).add_trace(
    px.scatter(
        df_train_move[df_train_move['City'] == 'Chicago'], 
        x='TotalTimeStopped_p50', y='DistanceToFirstStop_p50', color='City', opacity=0.3, color_discrete_map={'Chicago':'green'}
    )['data'][0], row=2, col=1
).add_trace(
    px.scatter(
        df_train_move[df_train_move['City'] == 'Philadelphia'], 
        x='TotalTimeStopped_p50', y='DistanceToFirstStop_p50', color='City', opacity=0.3, color_discrete_map={'Philadelphia':'purple'}
    )['data'][0], row=2, col=2
).update_layout(
    title='Max of TotalTimeStopped_p50 vs Max of DistanceToFirstStop_p50 by Intersection and Movements by City'
).update_xaxes(
    title_text='TotalTimeStopped_p50', row=2, col=1
).update_xaxes(
    title_text='TotalTimeStopped_p50', row=2, col=2
).update_yaxes(
    title_text='DistanceToFirstStop_p50', row=1, col=1
).update_yaxes(
    title_text='DistanceToFirstStop_p50', row=2, col=1
)


# In[ ]:


df_train_move_hour = df_train.groupby(
    ['Hour','IntersectionId','EntryHeading','ExitHeading','City'], as_index=False
)[['TotalTimeStopped_p50','DistanceToFirstStop_p50']].max().sort_values('City').reset_index(drop=True)


# In[ ]:


df_train_move_hour


# In[ ]:


px.scatter(
    df_train_move_hour[df_train_move_hour['City'] == 'Atlanta'], 
    x='TotalTimeStopped_p50', y='DistanceToFirstStop_p50', color='Hour', opacity=0.3
).update_layout(title='Max of TotalTimeStopped_p50 vs Max of DistanceToFirstStop_p50 by Intersection and Movements by Hour')


# In[ ]:


px.scatter(
    df_train.groupby(
        ['Hour','IntersectionId','City'], as_index=False
    )[['TotalTimeStopped_p50','DistanceToFirstStop_p50']].max().sort_values('City').reset_index(drop=True),
    x='TotalTimeStopped_p50', y='DistanceToFirstStop_p50', color='Hour', opacity=0.3
)


# In[ ]:


# GENERAL NOTES & FINDINGS

# TOTAL TIME STOPPED:
# Most of data is 0 - no delay
# Long tail distribution - somewhat exponentially lower probability for longer stop time
# CHECK EXTREMELY HIGH VALUE LOCATIONS AND TIME / MONTH

# vs HOUR:
# Day time and afternoon tends to have higher stop time
# Philadelphia has high stop time in Hour 23 - CHECK IF THIS IS REASONABLE

# vs MONTH:
# Jan - May should be removed
# Dec is not necessarily low in stop time - KEEP IT

# vs WEEKEND & HOUR:
# Generally, weekend has less stop time
# Chicago has much lower stop time in weekend compared to weekday
# Overall afternoon seems to be busiest time in weekend which is similar to the trend in weekday

# DISTANCE:
# Most of data is 0
# Low kurtois and long tail distribution - high concentration near median value for stop distance (reaching next intersection??)
# CHECK EXTREMELY LONG DISTANCE LOCATION AND TIME / MONTH

# vs HOUR:
# There seems to be small relationship with Hour - HOUR MAY BE EXCLUDED FROM FEATURES TO PREDICT DISTANCE??

# vs MONTH:
# Doesn't seem to have much relationship with month

# vs WEEKEND & HOUR:
# Overall less distance on weekend except Philadelphia

# TIME vs DISTANCE:
# These 2 seem to have positive correlations
# Relationship of Time and Distance is different by City
# Outliers are seen where Distance is long but Time is short for small observations - WORTH INVESTIGATING IF THESE ARE REASONABLE

# MOVEMENTS:
# It is difficult to say there is any clear relationship between turn movements and stop times and distance
# The relationship may appear if data is filtered by peak and non-peak directions


# NEW FEATURES
# Gravity point per City and calc distance to it from each data point as new feature??? - NOT REALLY, EVEN REGIONAL SITES HAVE SEVERE DELAY
# Paths would actually be meaningful from observation of spatial plots - SAME CORRIDOR SEEM TO HAVE SOMEWHAT SIMILAR DELAY 
# Number of approaches by unique number of EntryHeading per IntersectionId

# TIME FROM FIRST STOP - Optional and excluded from the initial analysis
# Most of data is 0
# Skewed distribution with long tail


# ### CONTINUE TO "eda_spatial.ipynb"
