#!/usr/bin/env python
# coding: utf-8

# ### DATA DESCRIPTION

# xxxxxx

# ### SCOPE OF THIS NOTEBOOK

# - Preparation of data fed to preprocessing.ipynb

# ### Import modules and data

# In[ ]:


import pandas as pd
import geopandas as gpd
import plotly.express as px
from geoalchemy2 import WKTElement


# In[ ]:


gdf_climate = gpd.read_file('../data/county-level-data/ClimDiv-edit.shp')
df_temp = pd.read_csv('../data/county-level-data/climdiv-tmpccy-v1.0.0-20200204.txt', delim_whitespace=True, header=None, dtype={0: 'str'})


# ### Process df_temp (temperature data)

# In[ ]:


df_temp.head()


# In[ ]:


df_temp.columns = ['code','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']


# In[ ]:


df_codes = pd.DataFrame(df_temp['code'].apply(lambda x: [
    x[0:2], # state_code
    x[2:5], # division_number
    x[5:7], # element_code
    x[7:] # year
]).tolist(), columns=['state_code','division_number','element_code','year'])


# In[ ]:


df_temp = pd.concat([df_temp, df_codes], join='inner', axis=1)


# In[ ]:


df_temp.head()


# #### Change temperature from fahrenheit to celsius

# In[ ]:


# Formula to convert fahrenheit to celsius
# c = (f-32)/1.8


# In[ ]:


# Convert all temperature unit
target_cols = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
df_temp.loc[:, target_cols] = (df_temp.loc[:, target_cols] -32) / 1.8


# In[ ]:


df_temp.head()


# ### Process gdf_climate to get State Name

# In[ ]:


gdf_climate.head()


# In[ ]:


gdf_climate = gdf_climate[['STATE','STATE_FIPS']].sort_values('STATE_FIPS')


# In[ ]:


gdf_climate = gdf_climate.drop_duplicates().reset_index(drop=True)


# In[ ]:


gdf_climate


# #### Filter only target states

# In[ ]:


states=['Georgia','Illinois','Massachusettes','Pennsylvania']
gdf_climate = gdf_climate[gdf_climate['STATE'].isin(states)]


# In[ ]:


gdf_climate


# ### Merge temperature and state data

# In[ ]:


df_temp = df_temp.merge(gdf_climate, how='inner', left_on='state_code', right_on='STATE_FIPS')


# In[ ]:


df_temp.info()


# In[ ]:


df_temp.describe()


# #### Filter only year 2018

# In[ ]:


df_temp = df_temp[df_temp['year'].astype(int) == 2018]


# #### Filter only Average Temperature

# In[ ]:


df_temp = df_temp[df_temp['element_code'] == '02']


# #### Unstack the dataframe

# In[ ]:


df_temp = df_temp.melt(
    id_vars=['division_number','STATE'], 
    value_vars=['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'], 
    var_name='month', 
    value_name='temp'
)


# In[ ]:


df_temp.head()


# In[ ]:


df_temp['STATE'].unique()


# In[ ]:


# Investigate teperature variabilities by divisions within same state by month
px.line(
    df_temp, 
    x='month', 
    y='temp', 
    color='division_number', 
    line_group='division_number',
    facet_row='STATE'
).update_layout(
    title='Average Temperature of state divisions by Month per State', 
    showlegend=False
)


# ### Calculate median temperature per State

# In[ ]:


df_temp_monthly = df_temp.groupby(['STATE','month'], as_index=False)['temp'].median()


# In[ ]:


df_temp_monthly


# ### Save the dataframe

# In[ ]:


df_temp_monthly.to_csv('../data/state_montyly_temperature.csv', index=False)

