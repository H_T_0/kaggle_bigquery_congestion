#!/usr/bin/env python
# coding: utf-8

# ### DATA DESCRIPTION

# The data consists of aggregated trip logging metrics from commercial vehicles, such as semi-trucks. The data have been grouped by intersection, month, hour of day, direction driven through the intersection, and whether the day was on a weekend or not.
# 
# For each grouping in the test set, you need to make predictions for three different quantiles of two different metrics covering how long it took the group of vehicles to drive through the intersection. Specifically, the 20th, 50th, and 80th percentiles for the total time stopped at an intersection and the distance between the intersection and the first place a vehicle stopped while waiting. You can think of your goal as summarizing the distribution of wait times and stop distances at each intersection.
# 
# Each of those six predictions goes on a new row in the submission file. Read the submission TargetId fields, such as 1_1, as the first number being the RowId and the second being the metric id. You can unpack the submission metric id codes with submission_metric_map.json.
# 
# The training set includes an optional additional output metric (TimeFromFirstStop) in case you find that useful for building your models. It was only excluded from the test set to limit the number of predictions that must be made.

# ### SCOPE OF THIS NOTEBOOK

# - EDA for categorical data in the training data

# ### Import modules and load training data

# In[ ]:


import os
import pandas as pd
import plotly
import plotly.express as px


# In[ ]:


pd.set_option('display.max_columns', None)


# In[ ]:


plotly.offline.init_notebook_mode()


# In[ ]:


px.set_mapbox_access_token('pk.eyJ1IjoiaHRhazEwIiwiYSI6ImNqZTB4a3RqazY1ajgyeHFocnk0b2NpOTUifQ.JL8YVn1zYcN3669YPLjohg')


# In[ ]:


os.listdir('../data/bigquery-geotab-intersection-congestion')


# In[ ]:


df_train = pd.read_csv('../data/bigquery-geotab-intersection-congestion/train.csv')


# ### Data Overview

# In[ ]:


df_train.head()


# In[ ]:


df_train.info()


# In[ ]:


for c in df_train.columns:
    if len(df_train[df_train[c].isna()==False]) < len(df_train):
        print('NULL EXIST IN COLUMN:', c)


# In[ ]:


df_train.describe()


# ### EDA for Categorical Data

# #### CITY

# In[ ]:


px.bar(
    df_train.groupby(['City'], as_index=False)['IntersectionId'].count(), x='City', y='IntersectionId', color='City'
).update_layout(title='Data Points per City')


# #### LOCATION

# In[ ]:


df_train_spatial = df_train.groupby(['IntersectionId','City'], as_index=False).agg({
    'Latitude':lambda x: x.iloc[0], 'Longitude':lambda x: x.iloc[0]
})


# In[ ]:


df_train_spatial


# In[ ]:


px.scatter_mapbox(
    df_train_spatial[df_train_spatial['City'] == 'Atlanta'], 
    lat='Latitude', lon='Longitude', color='City', opacity=0.3, zoom=10, color_discrete_map={'Atlanta':'blue'}
).update_layout(title='Data Locations in Atlanta', margin={'t':40, 'r':0, 'b':0, 'l':0})


# In[ ]:


px.scatter_mapbox(
    df_train_spatial[df_train_spatial['City'] == 'Boston'], 
    lat='Latitude', lon='Longitude', color='City', opacity=0.3, zoom=10, color_discrete_map={'Boston':'red'}
).update_layout(title='Data Locations in Boston', margin={'t':40, 'r':0, 'b':0, 'l':0})


# In[ ]:


px.scatter_mapbox(
    df_train_spatial[df_train_spatial['City'] == 'Chicago'], 
    lat='Latitude', lon='Longitude', color='City', opacity=0.3, zoom=9, color_discrete_map={'Chicago':'green'}
).update_layout(title='Data Locations in Chicago', margin={'t':40, 'r':0, 'b':0, 'l':0})


# In[ ]:


px.scatter_mapbox(
    df_train_spatial[df_train_spatial['City'] == 'Philadelphia'], 
    lat='Latitude', lon='Longitude', color='City', opacity=0.3, zoom=10, color_discrete_map={'Philadelphia':'purple'}
).update_layout(title='Data Locations in Philadelphia', margin={'t':40, 'r':0, 'b':0, 'l':0})


# #### EntryStreetName ExitStreetName

# In[ ]:


'ENTRY NAME:', len(df_train['EntryStreetName'].unique()), 'EXIT NAME:', len(df_train['ExitStreetName'].unique())


# #### EntryHeading	ExitHeading

# In[ ]:


'ENTRY:', sorted(df_train['EntryHeading'].unique().tolist()), 'EXIT:', sorted(df_train['ExitHeading'].unique().tolist())


# In[ ]:


df_train.groupby(['EntryHeading','ExitHeading'], as_index=False)['IntersectionId'].count()


# In[ ]:


px.parallel_categories(
    data_frame=df_train.loc[:, ['IntersectionId', 'EntryHeading', 'ExitHeading']],
    dimensions=['EntryHeading', 'ExitHeading']
).update_layout(title='Data Points per Entry & Exit Direction Pairs')


# #### HOUR

# In[ ]:


px.histogram(df_train, x='Hour', facet_row='City', color='City').update_layout(title='Data Points per Hour by City')


# #### MONTH

# In[ ]:


px.histogram(df_train, x='Month', facet_row='City', color='City').update_layout(title='Data Points per Month by City')


# #### WEEKEND

# In[ ]:


px.bar(
    df_train.groupby(['City','Weekend'],as_index=False)['IntersectionId'].count(), 
    x='City', y='IntersectionId', color='Weekend', barmode='relative'
).update_layout(title='Data Points per Weekend by City')


# #### PATH

# In[ ]:


len(df_train['Path'].unique())


# In[ ]:


len(df_train['IntersectionId'].unique())


# In[ ]:


df_train_path_sample = df_train[df_train['Path'].isin(df_train['Path'].unique()[:5])].groupby(['Path','IntersectionId'], as_index=False).agg({
        'Latitude': lambda x: x.iloc[0], 'Longitude': lambda x: x.iloc[0]
    })


# In[ ]:


px.line_mapbox(
    df_train_path_sample, lat='Latitude', lon='Longitude', line_group='Path', color='Path', zoom=12
).update_layout(title='Path Sample Locations', margin={'t':40, 'r':0, 'b':40, 'l':0}, legend_orientation='h')


# In[ ]:


# GENERAL NOTES & FINDINGS

# LOCATION:
# Generally, data is very clean and all data locations seem to make sense

# ENTRY STREET NAME & EXIT STREET NAME:
# TAbout 1,700 unique values (names)
# Some NaN - IMPUTE STREET NAME BY USING INTERSECTIONID + ENTRY EXIT HEADING

# ENTRY HEADING & EXIT HEADING:
# 8 directions and consistent between entry and exit directions
# Mostly through movements, and there are some u-turns recorded
# total 64 combinations of entry and exit (8 x 8), which make sense but too many as categorical data - CONVERT TO TURN (include U-TURN) & USE STREET NAME AS FEATURE

# HOUR:
# Number of data points look correlated with traffic volumes or delay - CHECK WITH DELAY DATA
# Do the time zones matter? Hour 0 is Hour 0 at each local zone? - DOUBLE CHECK BY DELAY
# Chicago has small number of data points in early morning - no action for now

# MONTH:
# Jan - May probably need to be removed from train data

# HOLIDAY:
# Do holidays follow local state holiday?
# Chicago has small number of data points of weekend - no action for now

# PATH:
# Path contains multiple intersections
# Over 15,000 unique paths
# Not to use for initial analysis


# TIME FROM FIRST STOP - Optional and excluded from the initial analysis


# ### CONTINUE TO "eda_numerical_data.ipynb"
