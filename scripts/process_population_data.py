#!/usr/bin/env python
# coding: utf-8

# ### DATA DESCRIPTION

# xxxxxx

# ### SCOPE OF THIS NOTEBOOK

# - Preparation of data fed to preprocessing.ipynb

# ### Import modules and data

# In[ ]:


import pandas as pd
import geopandas as gpd
#import plotly.express as px
from geoalchemy2 import WKTElement
from shapely.ops import transform 


# In[ ]:


df_pop = pd.read_csv('../data/us-population-by-zip-code/population_by_zip_2010.csv', dtype={'zipcode':'str'})


# In[ ]:


zipcode_Georgia = pd.read_csv('../data/state_zipcode_list/georgia.txt', sep=',', dtype={'zipcode':'str'})
zipcode_Illinois = pd.read_csv('../data/state_zipcode_list/illinois.txt', sep=',', dtype={'zipcode':'str'})
zipcode_Massachusetts = pd.read_csv('../data/state_zipcode_list/massachusetts.txt', sep=',', dtype={'zipcode':'str'})
zipcode_Pennsylvania = pd.read_csv('../data/state_zipcode_list/pennsylvania.txt', sep=',', dtype={'zipcode':'str'})


# In[ ]:


gdf_zip = gpd.read_file('../data/tl_2015_us_zcta510/tl_2015_us_zcta510.shp')


# In[ ]:


# load climate boundary data to filter the zipcode data by state
#gdf_climate = gpd.read_file('../data/county-level-data/ClimDiv-edit.shp').dissolve(by='STATE', as_index=False)


# In[ ]:


df_pop.info()


# In[ ]:


df_pop.describe()


# In[ ]:


df_pop


# #### Aggregate population by zipcode

# In[ ]:


df_pop_zip = df_pop.groupby(['zipcode'], as_index=False)['population'].sum()


# In[ ]:


#px.choropleth_mapbox(df_pop_zip, locations='zipcode')


# #### Filter population dataframe based on zipcode of the target states

# In[ ]:


df_pop_zip = df_pop_zip[
    df_pop_zip['zipcode'].isin(zipcode_Georgia['zipcode']) | 
    df_pop_zip['zipcode'].isin(zipcode_Illinois['zipcode']) | 
    df_pop_zip['zipcode'].isin(zipcode_Massachusetts['zipcode']) | 
    df_pop_zip['zipcode'].isin(zipcode_Pennsylvania['zipcode'])
]


# In[ ]:





# In[ ]:


len(df_pop_zip)


# ### Merge with ZIP code boundary

# In[ ]:


gdf_zip.crs


# In[ ]:


df_pop_zip = df_pop_zip.merge(
    gdf_zip[['ZCTA5CE10','geometry']], 
    how='inner', 
    left_on='zipcode', 
    right_on='ZCTA5CE10'
).drop(['ZCTA5CE10'], axis=1)


# In[ ]:


gdf_zip


# In[ ]:


df_pop_zip.info()


# ### Update CRS and swap lat and lon

# In[ ]:


gdf_pop_zip = gpd.GeoDataFrame(df_pop_zip, crs={'init': 'epsg:4269'}, geometry='geometry')


# In[ ]:


gdf_pop_zip = gdf_pop_zip.to_crs({'init':'epsg:4326'})


# In[ ]:


gdf_pop_zip['geometry'] = gdf_pop_zip['geometry'].apply(lambda poly: transform(lambda x,y: (y, x), poly))


# ### Save as shapefile

# In[ ]:


gdf_pop_zip.to_file('../data/zipcode_population.shp', driver='ESRI Shapefile')

