# Base image
FROM python:3.9-slim

# Update os and install git
RUN apt-get update && \
    apt-get install -y git && \
    apt-get install -y build-essential && \
    apt-get install -y libgdal-dev

# Install pip and setuptools
RUN pip install --upgrade pip && \
    pip install --upgrade setuptools

# Install dependecies
WORKDIR /home
COPY . /home
RUN pip install -r requirements.txt
