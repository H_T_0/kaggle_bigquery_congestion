/*
custom.js

This is a custom JavaScript file to add user interactivity on the app.
*/

// Table of Contents
$(document).ready(function() {
    $('.toc-list a').hover(
        function() {
            $(this).css({
                "color": "rgb(0, 150, 200)",
            })
    })
})