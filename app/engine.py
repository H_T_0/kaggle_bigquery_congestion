# Dashboard components
# RMSE for 6 metrics (Table)
# Correlation scatter plot - widget to change metric
# Accuracy by City, Hour, Month, Weekend
# Accuracy by Movements
# Accuracy by Location Comparison of 2 modelling methods
# Prediction results for test dataset - submit to kaggle to get score...
# Summary findings and potential improvements

import os
import pandas as pd
import plotly.express as px
from sklearn.metrics import mean_squared_error
from math import sqrt

px.set_mapbox_access_token('pk.eyJ1IjoiaHRhazEwIiwiYSI6ImNqZTB4a3RqazY1ajgyeHFocnk0b2NpOTUifQ.JL8YVn1zYcN3669YPLjohg')

# Path to data files (to be updated)
data_path = '../data'

# Metric columns (constants)
col_test = [
    'TotalTimeStopped_p20',
    'TotalTimeStopped_p50',
    'TotalTimeStopped_p80',
    'DistanceToFirstStop_p20',
    'DistanceToFirstStop_p50',
    'DistanceToFirstStop_p80'
    ] 
col_pred = [
    'pred_TotalTimeStopped_p20',
    'pred_TotalTimeStopped_p50',
    'pred_TotalTimeStopped_p80',
    'pred_DistanceToFirstStop_p20',
    'pred_DistanceToFirstStop_p50',
    'pred_DistanceToFirstStop_p80'
    ] 


# Load modelling result datasets and prepare dashboard data
def load_model_result():

    # Load model results - training data
    df_x_train = pd.read_csv(os.path.join(data_path, 'x_train.csv'))
    df_y_train = pd.concat([
        pd.read_csv(os.path.join(data_path, 'y1_time_train.csv')),
        pd.read_csv(os.path.join(data_path, 'y2_time_train.csv')),
        pd.read_csv(os.path.join(data_path, 'y3_time_train.csv')),
        pd.read_csv(os.path.join(data_path, 'y1_dist_train.csv')),
        pd.read_csv(os.path.join(data_path, 'y2_dist_train.csv')),
        pd.read_csv(os.path.join(data_path, 'y3_dist_train.csv'))
        ], join='inner', axis=1)

    df_pred_linear_regression = pd.read_csv(os.path.join(data_path, 'y_pred_linear_regression.csv'))
    df_pred_xgboost = pd.read_csv(os.path.join(data_path, 'y_pred_xgboost.csv'))

    # Load model results - test data
    #df_x_test = pd.read_csv(os.path.join(data_path, 'x_test.csv')) - this to be used when prediction is made for test data
    #df_pred_linear_regression = pd.read_csv(os.path.join(data_path, 'y_pred_linear_regression.csv'))
    #df_pred_xgboost = pd.read_csv(os.path.join(data_path, 'y_pred_xgboost.csv'))

    # Label model method
    df_pred_linear_regression['model'] = 'Linear Regression'
    df_pred_xgboost['model'] = 'XGBoost Regression'

    # Merge train data and prediction result
    df_train_lreg = pd.concat([df_x_train, df_y_train, df_pred_linear_regression], join='inner', axis=1)
    df_train_xgb = pd.concat([df_x_train, df_y_train, df_pred_xgboost], join='inner', axis=1)

    # Union result dataframe for different models
    df_train_result = pd.concat([df_train_lreg, df_train_xgb], join='inner', axis=0)

    '''
    df_x_train:

        index|  feature_1| feature_2|   ... DistanceToFirstStop_p80|
            0|          1|         0|   ...                     100|
            1|          0|         1|   ...                     200|
            2|          0|         0|   ...                      30|
            :|          :|         :|   ...                       :|
            n|          0|         1|   ...                      10|

    df_pred:

                        index|     pred_TotalTimeStopped_p20| ... pred_DistanceToFirstStop_p80|              model|
                            0|                          10.0| ...                         80.0|  Linear Regression|
                            :|                             :| ...                            :|                  :|
        n * number of models|                         100.0| ...                        300.0| XGBoost Regression|
    '''

    return df_train_result


def produce_RMSE(df_results):
    
    rmse_results = []

    # Filter by model method
    for m in df_results['model'].unique():
        df = df_results[df_results['model'] == m]

        # Calculate RMSE
        list_rmse = []
        for t, p in zip(col_test, col_pred):
            rmse = sqrt(mean_squared_error(df[t], df[p]))
            list_rmse.append(rmse)

        list_rmse.insert(0, m)
        '''
        [            'model',   'Time_p20', 'Time_p50', 'Time_p80', 'Dist_p20', 'Dist_p50', 'Dist_p80']
        ['Linear Regression',         50.0,      100.0,      250.0,       80.0,      150.0,      300.0]
        '''
        # Append rmse values for a model method
        rmse_results.append(list_rmse)

    # Create rmse result dataframe
    df_rmse = pd.DataFrame(rmse_results, columns=[
        'model',
        'TotalTimeStopped_p20',
        'TotalTimeStopped_p50',
        'TotalTimeStopped_p80',
        'DistanceToFirstStop_p20',
        'DistanceToFirstStop_p50',
        'DistanceToFirstStop_p80'
    ])

    return df_rmse


    # Create plot data
    fig = px.scatter(df_results, x=col_test, y=col_pred, trendline='ols', trendline_color_override='green', opacity=0.6)
    

def scatter_plots(df_results, x_col, y_col):
    # Create plot data
    return px.scatter(
                df_results, 
                x=x_col, y=y_col, 
                trendline='ols', 
                trendline_color_override='green', 
                opacity=0.6
                )


# Execute functions
def iniitialise_model_results():

    df_train_result = load_model_result()

    df_rmse = produce_RMSE(df_train_result)

    intial_plot = scatter_plots(df_train_result, 'TotalTimeStopped_p20', 'pred_TotalTimeStopped_p20')

    return df_train_result, df_rmse, intial_plot