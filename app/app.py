import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from engine import *


# Create app
app = dash.Dash(__name__)


# Load jquery
app.scripts.append_script({
    'external_url': 'https://code.jquery.com/jquery-3.2.1.min.js'
})


# App layout
app.layout = html.Div(className='container', children=[
    
    # Header
    html.Header(className='app-header', children=[
        html.Div(className='app-header-name', children=[html.P('HAYATO|TP|DS')]),
        html.Div(className='app-header-about', children=[html.P('ABOUT')])
    ]),

    # Container Body
    html.Div(className='container-body', children=[
        
        # App Title
        html.Div(className='app-title', children=[html.H1('Kaggle Congestion Competition')]),

        # Top Tabs
        html.Div(className='tabs', children=[
            # Tab Container
            dcc.Tabs(id="top-tabs", parent_className='top-tabs-parent', className='top-tabs-class', value='contents', children=[
                # Tabs
                dcc.Tab(className='tab', selected_className='tab--selected', label='1.Contents', value='contents'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='2.EDA for Categorical Data', value='categorical'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='3.EDA for Numerical Data', value='numerical'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='4.EDA for Spatial Data', value='spatial'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='5.Additional Data (Population)', value='additional-data'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='6.Pre-processing and Feature Development', value='processing'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='7.Modelling', value='modelling'),
                dcc.Tab(className='tab', selected_className='tab--selected', label='8.Result Dashboard', value='dashboard')
            ]),
            # Tab Contents
            html.Div(id='tab-content')
        ]),
    ]),

    # App Footer
    html.Footer(className='app-footer', children=[
        html.Div(className='app-footer-links', children=[
            html.P('Author: Hayato Takasaki'),
        ])
    ])
    
])


# Callbacks

# Tab rendering
@app.callback(Output('tab-content', 'children'),
              [Input('top-tabs', 'value')])
def render_content(tab):
    # Table of Contents, Background, Workflow
    if tab == 'contents':
        return html.Div(className='toc-background', children=[
                    html.Div(className='toc', children=[
                        html.H2('Contents'),
                        html.Div(className='toc-list', children=[
                            html.Ul(html.A('Background and Objectives')),
                            html.Ul(html.A('Project Workflow')),
                            html.Ul(html.A('Data Inventory')),
                            html.Ul(html.A('EDA for Categorical Data')),
                            html.Ul(html.A('EDA for Numerical Data')),
                            html.Ul(html.A('EDA for Spatial Data')),
                            html.Ul(html.A('Additional Data (Population)')),
                            html.Ul(html.A('Additional Data (Climate)')),
                            html.Ul(html.A('Pre-processing and Feature Development')),
                            html.Ul(html.A('Modelling')),
                            html.Ul(html.A('Model Evaluation'))
                        ])
                    ]),
                    html.Div(className='background', children=[
                        html.H2('Background and Objectives'),
                        html.P('A data science competition was held on Kaggle for the analysis of traffic congestion data in the selected US cities. Although the competition has already closed, I find this very interesting project to work on as it relates to my day to day work and potential application to it.'),
                        html.P('I aimed to use this competition as a learning and demonstration opportunity for relevant skills of data science in the area of transport. Particularly, I wanted to deliver the following tasks:'),
                        dcc.Markdown('''
                        - Exploratory data analysis
                        - Effective visualisation including spatial plots
                        - Model feature development
                        - Evaluation of model performance with different algorithms
                        ''')
                    ]),
                    # Workflow
                    html.Div(className='workflow', children=[
                        html.H2('Project Workflow'),
                        html.H3('Description:'),
                        html.P('''
                        The data analytics pipeline has been developed on top of Apache Airflow.
                        The individual project milestones are composed as scripts which are produced from Jupyter Notebooks.
                        These milestones are chained in a pipeline for exploratory data analysis, external data collection, data pre-processing, feature development, feature selection, modelling and model evaluation.
                        '''),
                        html.P('INSERT IMAGE HERE...')#html.Img()
                    ])
                ])
    # EDA Categorical Data
    elif tab == 'categorical':
        return html.Div(className='section', children=[
                    html.H2('Exploratory Data Analysis (Categerical Data)'),
                    html.Iframe(className='notebook', src=app.get_asset_url('eda_categorical_data.html'))
                ])
    # EDA Numerical Data
    elif tab == 'numerical':
        return html.Div(className='section', children=[
                    html.H2('Exploratory Data Analysis (Numerical Data)'),
                    html.Iframe(className='notebook', src=app.get_asset_url('eda_numerical_data.html'))
                ])
    # EDA Spatial Data
    elif tab == 'spatial':
        return html.Div(className='section', children=[
                    html.H2('Exploratory Data Analysis (Spatial Data)'),
                    html.Iframe(className='notebook', src=app.get_asset_url('eda_spatial.html')),
                    html.H4('Output Maps'),
                    html.Iframe(className='notebook', src=app.get_asset_url('intersection_locations_Atlanta.html')),
                    html.Iframe(className='notebook', src=app.get_asset_url('intersection_locations_Boston.html')),
                    html.Iframe(className='notebook', src=app.get_asset_url('intersection_locations_Chicago.html')),
                    html.Iframe(className='notebook', src=app.get_asset_url('intersection_locations_Philadelphia.html'))
                ])
    # Additional Data (Population and Climate)
    elif tab == 'additional-data':
        return html.Div(className='additional-data', children=[
                    html.Div(className='section', children=[
                        html.H2('Additional Data (Population)'),
                    ]),
                    html.Div(className='section', children=[
                        html.H2('Additional Data (Climate)'),
                    ])
                ])
    # Preprocessing and Feature Development
    elif tab =='preprocessing':
        return html.Div(className='section', children=[
                    html.H2('Preprocessing and Feature Development'),
                ])
    # Modelling and Evaluation
    elif tab == 'modelling':
        return html.Div(className='modelling', children=[
                    html.Div(className='section', children=[
                        html.H2('Modelling'),
                    ]),
                    html.Div(className='section', children=[
                        html.H2('Model Evaluation'),
                    ])
                ])
    # Result Dashboard
    elif tab == 'dashboard':
        # Prepare datasets for initial dashboard
        df_train_result, df_rmse, intial_plot = iniitialise_model_results()

        # Return the tab contents
        return html.Div(className='dashboard', children=[
                    html.Div(className='section', children=[
                        html.H2('Result Dashboard'),
                        html.Table([
                            html.Thead(
                                html.Tr([html.Th(col) for col in df_rmse.columns])
                            ),
                            html.Tbody([
                                html.Tr([
                                    html.Td(df_rmse.iloc[i][col]) for col in df_rmse.columns
                                ]) for i in range(0, len(df_rmse))
                            ])
                        ]),
                        dcc.Graph(id='scatter-error', figure=intial_plot)
                    ])
                ])


# Model Result Dashboard
# @app.callback(Output(),[Input()])

if __name__ == '__main__':
    app.run_server(debug=True, port=8050)